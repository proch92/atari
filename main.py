import torch
import torch.nn as nn
import net

import gym

games = ['Pong-v0', 'Breakout-v0']


def main():
	model = net.Net(games=len(games))

	for game in games:
		env = gym.make(game)
		obs = env.reset()

		gameover = False
		while (!gameover):
			env.render()


if __name__ == '__main__':
	main()