import torch
from torch import nn
from skimage import color
from sklearn import preprocessing as prep
import numpy as np

observation_space = (210, 160, 3)
action_space = (6)

class Net(nn.Model):
	"""docstring for a3c"""
	def __init__(self, games=0):
		super(Net, self).__init__()
		self.games = games

		self.lr = 1e-4

		self.cnn = nn.Sequential(
				nn.Conv2d(1, 16, 4, stride=(3,2), padding=1), # 70x80
				nn.ReLu(),
				nn.Conv2d(16, 32, 3, stride=2, padding=1), # 35x40
				nn.ReLu(),
				nn.Conv2d(32, 32, 3, stride=2, padding=1), # 18x20
				nn.ReLu()
			)

		self.fcn = nn.Sequential(
				nn.Linear(11522, 750),
				nn.ReLu(),
				nn.Dropout(p=0.25),
				nn.Linear(750, 200),
				nn.ReLu(),
				nn.Dropout(p=0.25)
			)

		self.valuenn = nn.Linear(200, 1)

		self.policynn = nn.Sequential(
				nn.Linear(200, 6),
				nn.Softmax(6)
			)

	def forward(self, obs, game):
		out = self.cnn(obs)
		out = out.view(-1, 11520) # 18x20x32 = 11520
		out = torch.cat([out, game]) # 11520 + 2 = 11522
		out = self.fcn(out)

		value = self.valuenn(out)
		policy = self.policynn(out)

		return value, policy
