import gym

games = ['Pong-v0', 'Breakout-v0']

for game in games:
	env = gym.make(game)

	print("action space: " + str(env.action_space))
	print("observation space: " + str(env.observation_space))

	obs = env.reset()

	print(type(obs))